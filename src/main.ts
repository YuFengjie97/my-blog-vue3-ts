import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "@/assets/public.scss";

import "font-awesome/css/font-awesome.min.css";

import "@/mock/mock.ts";

createApp(App)
  .use(store)
  .use(router)
  .mount("#app");
