import {
  createRouter,
  createWebHistory,
  createWebHashHistory,
  RouteRecordRaw,
} from "vue-router";
import Home from "../views/Home.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
    children: [
      {
        path: "/echarts",
        component: () => import("@/views/Echarts.vue"),
        children: [],
      },
      {
        path: "/css",
        component: () => import("@/views/Css.vue"),
        children: [],
      },
      {
        path: "/openlayer",
        component: () => import("@/views/OpenLayer.vue"),
      },
    ],
  },
  {
    path: "/me",
    component: () => import("@/views/me/Me.vue"),
  },
  {
    path: "/cssLayout",
    component: () => import("@/views/cssLayout/index.vue"),
  },
  {
    path: "/cssLayout2",
    component: () => import("@/views/cssLayout/index2.vue"),
  },
  {
    path: "/cssLayout3",
    component: () => import("@/views/cssLayout/index3.vue"),
  },
  {
    path: "/tailwind1",
    component: () => import("@/views/tailwind/index.vue"),
  },
  {
    path: "/test",
    component: () => import("@/views/test.vue"),
  },
];

const router = createRouter({
  // history: createWebHistory(process.env.BASE_URL),
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
});

export default router;
