# 容器属性

```css
/* 数值定义 */
.con {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  grid-template-rows: 100px 100px 100px;
}

/* repeat() */
.con {
  display: grid;
  grid-template-columns: repeat(3, 33.33%);
  grid-template-rows: repeat(3, 33.33%);
}

/* 按照某种模式 */
grid-template-columns: repeat(2, 100px 20px 80px);

/* 每列100px,自动填充 */
grid-template-columns: repeat(auto-fill, 100px);

/* 分两列,每列1:1*/
grid-template-columns: 1fr 1fr;

/* 倍数加入固定值 */
grid-template-columns: 150px 1fr 2fr;

/* minmax() */
grid-template-columns: 1fr 1fr minmax(100px, 1fr);

/* auto */
grid-template-columns: 100px auto 100px;

/* 网格线名称 */
.con {
  display: grid;
  grid-template-columns: [c1] 100px [c2] 100px [c3] auto [c4];
  grid-template-rows: [r1] 100px [r2] 100px [r3] auto [r4];
}

/* 间距 */
.container {
  grid-row-gap: 20px;
  grid-column-gap: 20px;
}

/* 区域 */
.container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  grid-template-rows: 100px 100px 100px;
  grid-template-areas:
    "a b c"
    "d e f"
    "g h i";
}

/* 子元素流向 */
grid-auto-flow: column;

/* 不在 grid-template-columns grid-template-rows元素的尺寸 */
grid-auto-rows: 50px;
grid-auto-columns: 50px;
```

# 项目属性

```css
/* item-1的宽度是从第二根线到第四根线 */
.item-1 {
  grid-column-start: 2;
  grid-column-end: 4;

  /* 等同于 */
  grid-column: 2 / 4;

  /* 从网格线1开始,默认跨越一个网格 */
  grid-column: 1;
}

/* span跨越,宽度是跨越2格 */
grid-column-start: span 2;
grid-column-end: span 2;

/* grid-area定位位置,将item-1定位在e */
.item-1 {
  grid-area: e;
}

/* grid-area: <row-start> / <column-start> / <row-end> / <column-end>; */
/* item-1区域为row,1-3;column,1-3 */
.item-1 {
  grid-area: 1 / 1 / 3 / 3;
}

/* 自己内容区定位 */
.item {
  justify-self: start | end | center | stretch;
  align-self: start | end | center | stretch;
}
```
